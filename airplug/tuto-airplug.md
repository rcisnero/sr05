# Tuto Airplug

- Via le tutoriel Airplug jusqu'à l'étape 27 (Chapter 1 et 2) [https://airplug-team.hds.utc.fr/en/doc/tuto/start](https://airplug-team.hds.utc.fr/en/doc/tuto/start)

# nest

## Communications

- En general, les applications peuvent se communiquer de manière local ou à distance.
- Les messages peuvent être de type broadcast ou directes (demande / response)
- Il n’y a pas d’ACK

### Principe de selection de messages :

| Reception by B | Sending by A | Sending by A |
| --- | --- | --- |
|  | to application B | to all applications |
| Locally | ok | B must subscribe to A |
| Remotely | B must subscribe to A | B must subscribe to A |
- stdin : reception (asynchrone, non bloquant)
- stdout : envoie
- stderr : messages de console
    - information
    - erreurs
    - warnings
    

## Message format

- what : payload (le contenu de message)
- who : à qui ? (ALL pour broadcast)
- where :
    - LHC : [localhost](http://localhost) (apps locales)
    - AIR : apps dans le voisinage
    - ALL : equivalent à LHC et AIR

Pour changer le format du message, on peut employer les options :

––what

––whatwho

––whatwhowhere

### Subscription et envoie de messages

[Link vers le tuto](https://airplug-team.hds.utc.fr/en/dwl/nest/info/tuto/step5)
