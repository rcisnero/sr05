import javax.swing.*;
import java.awt.*;

/**
 * Cette class gère l'interface graphique et modifie les parametres de la classe principale "ioAsync"
 */
public class ioInterface {
    /**
     * @param ioApp c'est l'instance de la classe principale "ioAsync"
     * @param regreshTime le temps d'attente entre chaque envoie de message
     */
    private final ioAsync ioApp;
    private int refreshTime = 1000;

    //Swing
    private final JTextField txtInputMessage = new JTextField("Message to send...");
    private final JLabel lblMessageReceived = new JLabel("");
    private final JTextField txtRefreshTime = new JTextField("1000");
    private final JButton btnSendMessage = new JButton("Send message");
    private final JButton btnSetLoop = new JButton("Loop");

    // Getters
    public int getRefreshTime() { return refreshTime; }
    public JLabel getLblMessageReceived() { return lblMessageReceived; }

    /**
     * Constructor. On initialise l'interface graphique avec ses composants et les listeners
     * @param io objet de la classe principale "ioAsync"
     */
    public ioInterface(ioAsync io) {
        this.ioApp = io;
        initialize();
        addListeners();
    }

    private void initialize() {
        JFrame root = new JFrame("Activité 4");;
        JPanel panel = new JPanel(new GridLayout(3,2));
        JLabel lblRefresh = new JLabel("Refresh time in ms ");

        // Ajout des éléments dans le JPanel
        panel.add(lblMessageReceived);
        panel.add(lblRefresh);
        panel.add(txtInputMessage);
        panel.add(txtRefreshTime);
        panel.add(btnSendMessage);
        panel.add(btnSetLoop);

        // Initialisation du JFrame
        root.add(panel);
        root.setSize(600, 300);
        root.setVisible(true);
        root.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * On ajoute des listeners sur :
     * - txtRefreshTime : mis à jour de l'attribut entier refreshTime utilisé par la classe "ioAsync"
     * - btnSetLoop : mis à jour de l'attribut booleen setLoop dans la classe "ioAsync"
     * - btnSendMessage : mis à jour de l'attribut String message dans la classe "ioAsync" pour écrire sur la sortie standard
     */
    private void addListeners() {
        txtRefreshTime.addActionListener(e -> refreshTime = Integer.parseInt(txtRefreshTime.getText()));
        btnSetLoop.addActionListener(e -> ioApp.setLoop(!ioApp.isLoop()));
        btnSendMessage.addActionListener(e -> {
            try {
                ioApp.setMessage(txtInputMessage.getText());
                ioApp.sendMessage();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        });
    }







}
