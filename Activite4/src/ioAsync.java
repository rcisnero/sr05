import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Cette classe va traiter l'écriture et lecture des messages en sortie standard.
 * Puisqu'il existe une interface graphique, la sortie erreur standard a été substituée
 * par un label qui va afficher le message reçu.
 */
public class ioAsync {

    /**
     * @param message C'est le message écrit sur l'interface graphique et affiché sur la sortie
     *                standard (dans le cas d'une pipeline double)
     * @param inputStream Le message écrit sur l'entrée standard et affiché sur l'interface graphique.
     * @param loop Indique si le programme est en boucle
     * @param ioApp instance de la classe de l'interface graphique
     */
    private String message;
    private final BufferedInputStream inputStream = (BufferedInputStream) System.in;
    private boolean loop = false;
    private final ioInterface ioApp;

    //Getters
    public boolean isLoop() { return loop; }

    //Setters
    public void setMessage(String message) { this.message = message; }
    public void setLoop(boolean loop) { this.loop = loop; }

    /**
     * Constructor.
     * Envoie l'instance principale à l'interface graphique
     */
    private ioAsync() {
        ioApp = new ioInterface(this);
    }

    /**
     * Écrit un message sur la sortie standard et on augment sa durée pour vérifier l'atomicité
     * @throws InterruptedException
     */
    public void sendMessage() throws InterruptedException {
        System.out.println(message);
        Thread.sleep(this.ioApp.getRefreshTime());
    }

    /**
     * Lit un message écrit sur l'entrée standard et l'affiche sur l'interface graphique
     * @param n C'est la taille du texte à lire envoyé par @param inputStream
     * @throws IOException
     */
    private void receiveMessage(Integer n) throws IOException, InterruptedException {
        byte[] messageReceived = new byte[n];
        inputStream.read(messageReceived);

        System.err.println("Reception de :" + new String(messageReceived));
        //ioApp.getLblMessageReceived().setText("Message received : " + new String(messageReceived) + "\n");

        for (int i = 0; i < 10; i++) {
            System.err.print(".");
            Thread.sleep(1000);
        }
        System.out.println("Fin reception");
    }

    /**
     * La boucle infinie qui va envoyer un message si on est en mode "loop" et qui va lire l'entrée standard
     * @throws InterruptedException
     * @throws IOException
     */
    private void initialize() throws InterruptedException, IOException {
        while (true) {
            Thread.sleep(100);
            if (isLoop()) sendMessage();
            if (inputStream.available() > 0) receiveMessage(inputStream.available());
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ioAsync io = new ioAsync();
        io.initialize();
    }

}
