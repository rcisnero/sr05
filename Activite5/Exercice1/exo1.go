package main

import (
	"flag"
	"fmt"
	"runtime"
)

// Retourne la valeur minimale entre deux enteirs
func min(a, b int) int {
    if a <= b {
        return a
    }
    return b
}

// Somme interne de chaque portion de tableau
func sumTab(slice []int, result_chan chan<- int)  {
	sum := 0
	for i := 0; i < len(slice); i++ {
		sum += slice[i]
	}
	// On envoie le resultat à travers du canal de communication avec main
	result_chan<- sum
}

func main() {
	// Obtention de la taille à partir de la ligne de commandes
	tab_size := flag.Int("n", 8, "Taille du tableau")
	flag.Parse()

	// Definition des variables et canaux
	tab := make([]int, *tab_size)
	var nb_cpu = runtime.NumCPU()
	result_chan := make(chan int)
	sum := 0

	// Remplissage du tableau
	for i := 0; i < *tab_size; i++ {
		tab[i] = i+1
	}

	// Determination de la taille de chaque portion de tableau
	// Si la division n'est pas exacte, on augmente la taille de chaque sous-tableau
	slice_size := *tab_size / nb_cpu
	if *tab_size % nb_cpu != 0 {
		slice_size += 1
	}

	// Division du tableau et envoi de la portion à sumTab
	// On incremente la taille de la portion à chaque itération
	for i := 0; i < len(tab); i += slice_size {
		// Avec la fonction min, on evite de prendre un indice superieur à la taille du tableau
        slice := tab[i:min(i+slice_size, len(tab))]
		go sumTab(slice, result_chan)
		sum += <-result_chan
    }

	fmt.Println("Somme = ", sum)
}
