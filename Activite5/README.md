# Exercice 1 - Somme.

Tout d'abord, on récupère la taille du tableau à sommer depuis la ligne de commandes et on remplit le tableau. La taille de chaque sous-tableau est obtenue avec une division de la taille du tableau original entre le nombre de processeurs de l'ordinateur. Puis, on divise le tableau selon la taille obtenue et on execute une go-routine afin de sommer les valeurs de chaque sous-tableau de manière parallèle. Finalement, le résultat est envoyé par le canal de communication et sommé avec les autres résultats de chaque sous-tableau.

On aperçoit un parallélisme du type SIMD (Single Instruction, Multiple Data), on a une seule instruction (la somme des valeurs internes) executée sur des différentes données (chaque sous tableau).

Utilisation : compiler le programme avec la commande go build exo1.go, puis l'exécuter avec ./exo1 -n X avec X comme la taille du tableau. ( ./exo1 -n 20 pour un tableau de taille 20).

Code :

    package main

    import (
        "flag"
        "fmt"
        "runtime"
    )

    // Retourne la valeur minimale entre deux enteirs
    func min(a, b int) int {
        if a <= b {
            return a
        }
        return b
    }

    // Somme interne de chaque portion de tableau
    func sumTab(slice []int, result_chan chan<- int)  {
        sum := 0
        for i := 0; i < len(slice); i++ {
            sum += slice[i]
        }
        // On envoie le resultat à travers du canal de communication avec main
        result_chan<- sum
    }

    func main() {
        // Obtention de la taille à partir de la ligne de commandes
        tab_size := flag.Int("n", 8, "Taille du tableau")
        flag.Parse()

        // Definition des variables et canaux
        tab := make([]int, *tab_size)
        var nb_cpu = runtime.NumCPU()
        result_chan := make(chan int)
        sum := 0

        // Remplissage du tableau
        for i := 0; i < *tab_size; i++ {
            tab[i] = i+1
        }

        // Determination de la taille de chaque portion de tableau
        // Si la division n'est pas exacte, on augmente la taille de chaque sous-tableau
        slice_size := *tab_size / nb_cpu
        if *tab_size % nb_cpu != 0 {
            slice_size += 1
        }

        // Division du tableau et envoi de la portion à sumTab
        // On incremente la taille de la portion à chaque itération
        for i := 0; i < len(tab); i += slice_size {
            // Avec la fonction min, on evite de prendre un indice superieur à la taille du tableau
            slice := tab[i:min(i+slice_size, len(tab))]
            go sumTab(slice, result_chan)
            sum += <-result_chan
        }

        fmt.Println("Somme = ", sum)
    }

# Exercice 2 - Crible.

Ce programme appelle une fonction récursive filtrer() avec le premier nombre premier 2, un canal avec les nombres jusqu'à la sélection de l'utilisateur, et un canal pour envoyer le résultat. C'est très important de fermer les canaux principaux et auxiliaires (crées lors de chaque appel récursif) car il y a des variables qui vérifient si un canal est toujours ouvert ou fermé.

À chaque itération, on vérifie si le nombre suivant dans le canal est divisible par "l'entête" de la fonction filtrer(), sinon on le vérifie dans une autre go-routine déjà créée. Dans le cas échéant, on fait un appel récursif avec le nouveau nombre premier comme entête, et deux canaux avec la suite des nombres. Lorsque le canal principal est fermé, on envoie tous les nombres premiers par le canal résultat et on ferme les canaux créés lors des appels récursifs. Finalement, on construit la chaîne à afficher avec le résultat.

On aperçoit un parallélisme du type MIMD (Multiple Instruction, Multiple Data), car chaque processeur exécute sa propre séquence d'instructions et il travaille dans une partie différente du problème.

Utilisation : compiler le programme avec la commande go build exo2.go, puis l'exécuter avec ./exo2 -n X avec X comme le nombre limite. ( ./exo2 -n 20 pour une vérification de 2 à 20).

Algorithme :

    filtrer(nb)
    lire canal
    multiple ?
    si oui -> rien à faire
    si non
        go_routine deja crée ?
        si oui -> lui envoyer(nb)
        sinon creer go_routine(nb)

Code :

    package main

    import (
        "flag"
        "fmt"
        "strconv"
        "time"
    )

    func filtrer(start int, nb_chan chan int, result_chan chan int)  {
        // On n'alloue pas de memoire pour les canaux car on crée la go-routine uniquement quand on trouve un nouveau nombre premier
        var filtrer_in chan int
        var filtrer_out chan int

        for{
            // La variable prime va lire le contenu du canal et isNext va verifier si le canal est toujours ouvert
            nb, isNext := <-nb_chan

            // Si le canal est ouvert...
            if isNext {
                // Le nombre est multiple ?
                if nb % start != 0 {

                    if filtrer_in != nil {
                        // La go-routine est déjà crée. On envoie la valeur à travers de son canal
                        filtrer_in <- nb
                    } else {
                        // La go-routine n'a pas été crée. Donc on alloue la memoire pour les canaux et on l'execute
                        filtrer_in = make(chan int)
                        filtrer_out = make(chan int)
                        go filtrer(nb, filtrer_in, filtrer_out)
                    }
                }

            // Si le canal est fermé et si on a reçu la valeur qui entraine la recuperation de resultats
            } else if (nb == 0){
                // On ajoute le nombre 2 au canal de nombres premiers
                result_chan <- start

                // Fermeture des canaux avec allocation mémoire
                if filtrer_in != nil {
                    close(filtrer_in)
                }

                // Recuperation des nombres premiers de chaque go-routine
                if filtrer_out != nil {
                    for i := range filtrer_out {
                        result_chan <- i
                    }
                }
                // Fin du programme avec la fermeture du canal des resultats
                // detecté par la variable more en fin du main
                close(result_chan)
                return
            }
        }
    }

    func main() {
        nb_max := flag.Int("n", 20, "Obtenir les nombres premiers jusqu'à un nombre n")
        flag.Parse()
        nb_chan := make(chan int)
        result_chan := make(chan int)
        
        // Demarrage de la fonction recursive avec le premier nombre premier
        go filtrer(2, nb_chan, result_chan)

        // On remplit le canal jusqu'au nombre saisi par l'utilisateur
        // Puis, on ferme le canal pour indiquer à la fonction filtrer que le canal est fermé (variable isNext)
        for i := 3; i <= *nb_max; i++ {
            nb_chan<-i
        }
        // On envoie la valeur qui entraine la recuperation de resultats (eg. 0)
        nb_chan<-0
        close(nb_chan)
            
        // Pause gourmande pour attendre la fin des go-routines
        time.Sleep(1*time.Second)

        // Obtention des nombres premiers du canal result
        // La variable res va lire le contenu du canal et more va verifier si le canal est toujours ouvert
        // Construction du message avec le resultat
        text := "Liste des nombres premiers jusqu'à " + strconv.Itoa(*nb_max) + " : "
        for{
            res, more := <-result_chan
            if more {
                text = text + strconv.Itoa(res) + " "
            } else { 
                fmt.Println(text)
                return 
            }
        }
        
    }

