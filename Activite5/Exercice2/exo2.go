package main

import (
	"flag"
	"fmt"
	"strconv"
	"time"
)

func filtrer(start int, nb_chan chan int, result_chan chan int)  {
	// On n'alloue pas de memoire pour les canaux car on crée la go-routine uniquement quand on trouve un nouveau nombre premier
	var filtrer_in chan int
	var filtrer_out chan int

	for{
		// La variable prime va lire le contenu du canal et isNext va verifier si le canal est toujours ouvert
		nb, isNext := <-nb_chan

		// Si le canal est ouvert...
		if isNext {
			// Le nombre est multiple ?
			if nb % start != 0 {

				if filtrer_in != nil {
					// La go-routine est déjà crée. On envoie la valeur à travers de son canal
					filtrer_in <- nb
				} else {
					// La go-routine n'a pas été crée. Donc on alloue la memoire pour les canaux et on l'execute
					filtrer_in = make(chan int)
					filtrer_out = make(chan int)
					go filtrer(nb, filtrer_in, filtrer_out)
				}
			}

		// Si le canal est fermé et si on a reçu la valeur qui entraine la recuperation de resultats
		} else if (nb == 0){
			// On ajoute le nombre 2 au canal de nombres premiers
			result_chan <- start

			// Fermeture des canaux avec allocation mémoire
			if filtrer_in != nil {
				close(filtrer_in)
			}

			// Recuperation des nombres premiers de chaque go-routine
			if filtrer_out != nil {
				for i := range filtrer_out {
					result_chan <- i
				}
			}
			// Fin du programme avec la fermeture du canal des resultats
			// detecté par la variable more en fin du main
			close(result_chan)
			return
		}
	}
}

func main() {
    nb_max := flag.Int("n", 20, "Obtenir les nombres premiers jusqu'à un nombre n")
	flag.Parse()
	nb_chan := make(chan int)
	result_chan := make(chan int)
	
	// Demarrage de la fonction recursive avec le premier nombre premier
	go filtrer(2, nb_chan, result_chan)

	// On remplit le canal jusqu'au nombre saisi par l'utilisateur
	// Puis, on ferme le canal pour indiquer à la fonction filtrer que le canal est fermé (variable isNext)
	for i := 3; i <= *nb_max; i++ {
		nb_chan<-i
	}
	// On envoie la valeur qui entraine la recuperation de resultats (eg. 0)
	nb_chan<-0
	close(nb_chan)
		
	// Pause gourmande pour attendre la fin des go-routines
	time.Sleep(1*time.Second)

	// Obtention des nombres premiers du canal result
	// La variable res va lire le contenu du canal et more va verifier si le canal est toujours ouvert
	// Construction du message avec le resultat
	text := "Liste des nombres premiers jusqu'à " + strconv.Itoa(*nb_max) + " : "
	for{
		res, more := <-result_chan
		if more {
			text = text + strconv.Itoa(res) + " "
		} else { 
			fmt.Println(text)
			return 
		}
	}
	
}
